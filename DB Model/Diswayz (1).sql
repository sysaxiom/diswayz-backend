CREATE TABLE `organization` (
  `id` [pk],
  `organization_name` varchar(255)
);

CREATE TABLE `shop` (
  `organization_id` integer,
  `id` [pk],
  `shop_name` varchar(255)
);

CREATE TABLE `shop_layout` (
  `id` [pk],
  `shop_id` integer,
  `layout_coordinates` json
);

CREATE TABLE `shop_aisle` (
  `id` [pk],
  `shop_id` integer,
  `aisle_coordinates` json
);

CREATE TABLE `rack` (
  `id` [pk],
  `shop_id` integer,
  `rack_name` varchar(255),
  `rack_coordinates` json,
  `is_special_area` varchar(255)
);

CREATE TABLE `bluetooth_devices` (
  `id` [pk],
  `shop_id` integer,
  `device_id` varchar(255),
  `lat` double,
  `lon` double
);

ALTER TABLE `shop` ADD CONSTRAINT `shop_organization` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`);

ALTER TABLE `shop` ADD CONSTRAINT `shop_layout` FOREIGN KEY (`id`) REFERENCES `shop_layout` (`shop_id`);

ALTER TABLE `shop` ADD CONSTRAINT `shop_aisle` FOREIGN KEY (`id`) REFERENCES `shop_aisle` (`shop_id`);

ALTER TABLE `shop` ADD CONSTRAINT `rack` FOREIGN KEY (`id`) REFERENCES `rack` (`shop_id`);

ALTER TABLE `shop` ADD CONSTRAINT `bluetooth_devices` FOREIGN KEY (`id`) REFERENCES `bluetooth_devices` (`shop_id`);
